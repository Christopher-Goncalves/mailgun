package com.mail.mailgun.controller;

import com.mail.mailgun.model.dto.Menssagem;
import com.mail.mailgun.model.dto.MenssagemComArquivo;
import com.mail.mailgun.service.MailGunService;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/mail")
public class MailgunController {
    private final MailGunService mailGunService;

    public MailgunController(MailGunService mailGunService) {
        this.mailGunService = mailGunService;
    }


    @PostMapping("/send")
    public ResponseEntity enviarEmail(@RequestBody @Valid Menssagem dto) {
        mailGunService.realizarEnvioDeEmail(dto);
        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/send/attachment")
    public ResponseEntity enviarEmailComArquivo(@RequestBody @Valid MenssagemComArquivo dto) {
        mailGunService.realizarEnvioDeEmailComArquivo(dto);
        return ResponseEntity.ok().build();
    }
}
