package com.mail.mailgun.model.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class Arquivo {
    @NotNull(message = "Campo 'base64' não pode ser vazio ou nulo")
    private String base64;
    @NotNull(message = "Campo 'nome' não pode ser vazio ou nulo")
    private String nome;

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
