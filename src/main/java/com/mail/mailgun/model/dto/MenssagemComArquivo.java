package com.mail.mailgun.model.dto;

import com.mail.mailgun.model.exeption.BadRequest;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public class MenssagemComArquivo {
    @NotNull(message = "Campo 'para' não pode ser vazio ou nulo")
    private List<String> para;
    @NotBlank(message = "Campo 'assunto' não pode ser vazio ou nulo")
    private String assunto;
    @NotBlank(message = "Campo 'texto' não pode ser vazio ou nulo")
    private String texto;
    private String de;
    private String template;

    private List<Arquivo> arquivo;

    public List<Arquivo> getArquivo() {
        return arquivo;
    }

    public void setArquivo(List<Arquivo> arquivo) {
        this.arquivo = arquivo;
    }

    public List<String> getPara() {
        return para;
    }

    public void setPara(List<String> para) {
        this.para = para;
    }

    public String getAssunto() {
        return assunto;
    }

    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getDe() {
        return de;
    }

    public void setDe(String de) {
        this.de = de;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public MenssagemComArquivo() {
    }

    public MenssagemComArquivo(List<String> para, String assunto, String texto, String de, String template) {
        this.para = para;
        this.assunto = assunto;
        this.texto = texto;
        this.de = de;
        this.template = template;
    }

    public void validarCampoPara() {
        if (this.para.isEmpty()) {
            throw new BadRequest("Campo 'PARA' não pode ser vazio ou nulo");
        }
    }
}
