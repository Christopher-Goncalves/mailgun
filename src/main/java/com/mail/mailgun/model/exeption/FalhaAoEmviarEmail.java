package com.mail.mailgun.model.exeption;

import java.util.function.Supplier;

public class FalhaAoEmviarEmail extends RuntimeException implements Supplier<FalhaAoEmviarEmail> {

    private String mensagem;

    public FalhaAoEmviarEmail(String message) {
        super(message);
        this.mensagem = message;
    }

    @Override
    public FalhaAoEmviarEmail get() {
        return new FalhaAoEmviarEmail(mensagem);
    }
}
