package com.mail.mailgun.service;

import com.mail.mailgun.model.dto.Arquivo;
import com.mail.mailgun.model.dto.Menssagem;
import com.mail.mailgun.model.dto.MenssagemComArquivo;
import com.mail.mailgun.model.exeption.FalhaAoEmviarEmail;
import com.mailgun.api.v3.MailgunMessagesApi;
import com.mailgun.client.MailgunClient;
import com.mailgun.exception.MailGunException;
import com.mailgun.model.message.Message;
import com.mailgun.model.message.MessageResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Objects;

@Service
public class MailGunService {
    //    private String API_MAILGUN_KEY ="3c3b97dde6426cc594a5f3a3ab8edd52-db137ccd-f17fdd12";
    private String API_MAILGUN_KEY = System.getenv("API_MAILGUN_KEY");
    //    private String API_MAILGUN_DOMAIN = "pradomstecnologia.com.br";
    private String API_MAILGUN_DOMAIN = System.getenv("API_MAILGUN_DOMAIN");
    private String API_MAILGUN_DAFULT_SENDER = System.getenv("API_MAILGUN_DAFULT_SENDER");

    public void realizarEnvioDeEmail(Menssagem dto) {
        MailgunMessagesApi mailgunMessagesApi = MailgunClient.config(API_MAILGUN_KEY)
                .createApi(MailgunMessagesApi.class);
        Message message = montarMensagem(dto);
        enviarEmail(message, mailgunMessagesApi);
    }

    public void realizarEnvioDeEmailComArquivo(MenssagemComArquivo dto)  {
        MailgunMessagesApi mailgunMessagesApi = MailgunClient.config(API_MAILGUN_KEY)
                .createApi(MailgunMessagesApi.class);
        List<File> arquivos = montarArquivos(dto.getArquivo());
        Message message = montarMensagemComArquivo(dto, arquivos);
        enviarEmail(message, mailgunMessagesApi);
        deletarArquivos(arquivos);
    }

    private void deletarArquivos(List<File> arquivos) {
        arquivos.forEach(ar -> ar.delete());
    }

    private List<File> montarArquivos(List<Arquivo> dto) {
        List<File> files = new ArrayList<>();
        dto.forEach(ar -> {
            String outputFileName = ar.getNome();
            byte[] data = Base64.getDecoder().decode(ar.getBase64().getBytes());
            File outputFile = new File(outputFileName);
            try (FileOutputStream fos = new FileOutputStream(outputFile)) {
                fos.write(data);
            } catch (IOException e) {
                e.printStackTrace();
            }
            files.add(outputFile);
        });

        return files;
    }

    private Message montarMensagem(Menssagem dto) {
        dto.validarCampoPara();
        return Message.builder()
                .from(String.format(Objects.nonNull(dto.getDe()) ? dto.getDe() : API_MAILGUN_DAFULT_SENDER))
                .to(dto.getPara()) //receber
                .subject(dto.getAssunto())// recebe
                .text(dto.getTexto())// recebe
                .template((Objects.nonNull(dto.getTemplate()) && !dto.getTemplate().isEmpty()) ? dto.getTemplate() : null)
                .build();
    }

    private Message montarMensagemComArquivo(MenssagemComArquivo dto, List<File> files) {
        dto.validarCampoPara();
        return Message.builder()
                .from(String.format(Objects.nonNull(dto.getDe()) ? dto.getDe() : API_MAILGUN_DAFULT_SENDER)) //setar variavel
                .to(dto.getPara()) //receber
                .subject(dto.getAssunto())// recebe
                .text(dto.getTexto())// recebe
                .attachment(files)
                .template((Objects.nonNull(dto.getTemplate()) && !dto.getTemplate().isEmpty()) ? dto.getTemplate() : null)// recebe
                .build();

    }

    private void enviarEmail(Message message, MailgunMessagesApi mailgunMessagesApi) {
        try {
            MessageResponse x = mailgunMessagesApi.sendMessage(API_MAILGUN_DOMAIN, message);
        } catch (MailGunException ex) {
            ex.printStackTrace();
            throw new FalhaAoEmviarEmail(ex.getMessage());
        }
    }
}
